from django import forms

from reviewer.models import Product, Review


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'description', 'category', 'product_image']


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ['review_text', 'grade', ]
