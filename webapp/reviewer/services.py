from reviewer.models import Product, Category, Review
from django.contrib.auth.models import User


def product_create(data: dict, category: Category) -> Product:
    return Product.objects.create(
        description=data.get('description'),
        name=data.get('name'),
        category=category,
        product_image=data.get('product_image')
    )


def review_create(data: dict, product: Product, user: User) -> Review:
    return Review.objects.create(
        review_text=data.get('review_text'),
        grade=data.get('grade'),
        product=product,
        author=user
    )
