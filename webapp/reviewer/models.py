from django.contrib.auth import get_user_model
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models import Avg


class Product(models.Model):
    name = models.CharField(max_length=150, null=False, blank=False)
    category = models.ForeignKey('reviewer.Category', on_delete=models.CASCADE, null=False, blank=False)
    description = models.TextField(max_length=3000, null=True, blank=True)
    product_image = models.ImageField(
        null=True, blank=True,
        upload_to='product_pics', verbose_name='Фотография продукта/услуги'
    )

    def get_average_grade(self):
        grade = Review.objects.all().filter(product=self).aggregate(Avg('grade')).get('grade__avg')
        if grade:
            return grade
        else:
            return 0


class Review(models.Model):
    author = models.ForeignKey(
        get_user_model(), on_delete=models.SET_DEFAULT,
        default=1, related_name='reviews',
        verbose_name='Автор'
    )
    product = models.ForeignKey(
        'reviewer.Product', related_name='reviews',
        on_delete=models.CASCADE, verbose_name='Продукт'
    )
    review_text = models.TextField(max_length=3000, null=False, blank=False)
    grade = models.PositiveSmallIntegerField(
        null=False, blank=False, verbose_name='Оценка', default=1,
        validators=[MinValueValidator(1), MaxValueValidator(5)]
    )


class Category(models.Model):
    category = models.CharField(max_length=30, null=False, blank=False)
