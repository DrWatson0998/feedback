from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import ListView, CreateView, DeleteView, UpdateView

from reviewer.forms import ReviewForm
from reviewer.models import Product, Review
from reviewer.services import review_create


class ReviewListView(ListView):
    context_object_name = 'reviews'
    paginate_by = 6
    model = Review


class ReviewCreateView(LoginRequiredMixin, CreateView):
    template_name = 'product/product_detail_view.html'
    model = Review
    form_class = ReviewForm

    def get(self, request, *args, **kwargs):
        context = {
            'product': get_object_or_404(Product, pk=kwargs.get('pk'))
        }
        return render(request, self.template_name, context=context)

    def get_success_url(self):
        return reverse('product_detail', kwargs={'pk': self.kwargs.get('pk')})

    def post(self, request, *args, **kwargs):
        product = get_object_or_404(Product, pk=kwargs.get('pk'))
        form = self.form_class(data=request.POST)
        if form.is_valid():
            review_create(request.POST, product, self.request.user)
            return redirect(self.get_success_url())
        return render(request, self.template_name, context={
            'product': product,
            'form': form
        })


class ReviewDeleteView(UserPassesTestMixin, DeleteView):
    model = Review

    def test_func(self):
        return self.request.user == self.get_object().author

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        return redirect(self.get_success_url())

    def get_success_url(self):
        return reverse('product_detail', kwargs={'pk': self.object.product.pk})


class ReviewUpdateView(UserPassesTestMixin, UpdateView):
    template_name = 'review/update_review_view.html'
    form_class = ReviewForm
    model = Review

    def test_func(self):
        return self.request.user == self.get_object().author

    def get_success_url(self):
        return reverse('product_detail', kwargs={'pk': self.object.product.pk})
