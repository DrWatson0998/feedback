from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import HttpRequest
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, CreateView, DetailView, DeleteView, UpdateView

from reviewer.forms import ProductForm
from reviewer.models import Product, Category
from reviewer.services import product_create


class ProductListView(ListView):
    template_name = 'product/product_list_view.html'
    context_object_name = 'products'
    paginate_by = 6
    model = Product


class ProductCreateView(PermissionRequiredMixin, CreateView):
    template_name = 'product/create_product_view.html'
    model = Product
    form_class = ProductForm
    permission_required = 'reviewer.add_product'

    def get_success_url(self):
        return reverse('products_list')

    def post(self, request: HttpRequest, *args, **kwargs):
        form = self.form_class(data=request.POST, files=request.FILES)
        if form.is_valid():
            category = form.cleaned_data.get('category')
            product_create(form.cleaned_data, category)
            return redirect(self.get_success_url())
        return render(request, self.template_name, context={
            'form': form
        })


class ProductDetailView(DetailView):
    template_name = 'product/product_detail_view.html'
    model = Product
    context_object_name = 'product'


class ProductDeleteView(PermissionRequiredMixin, DeleteView):
    model = Product
    success_url = reverse_lazy('products_list')
    permission_required = 'reviewer.delete_product'


class ProductUpdateView(PermissionRequiredMixin, UpdateView):
    template_name = 'product/update_product_view.html'
    form_class = ProductForm
    model = Product
    permission_required = 'reviewer.change_product'

    def get_success_url(self):
        return reverse('product_detail', kwargs={'pk': self.get_object().pk})
