from django.urls import path

from reviewer.views.product_view import (
    ProductListView, ProductCreateView,
    ProductDetailView, ProductDeleteView, ProductUpdateView,
)
from reviewer.views.review_view import (
    ReviewCreateView, ReviewDeleteView, ReviewUpdateView
)

urlpatterns = []


review_urls = [
    path('product/<int:pk>/review/create', ReviewCreateView.as_view(), name='create_review'),
    path('review/<int:pk>/delete', ReviewDeleteView.as_view(), name='delete_review'),
    path('review/<int:pk>/update', ReviewUpdateView.as_view(), name='update_review'),
]

product_urls = [
    path('', ProductListView.as_view(), name='products_list'),
    path('product/create', ProductCreateView.as_view(), name='create_product'),
    path('product/<int:pk>/', ProductDetailView.as_view(), name='product_detail'),
    path('product/<int:pk>/delete', ProductDeleteView.as_view(), name='delete_product'),
    path('product/<int:pk>/update', ProductUpdateView.as_view(), name='product_update'),
]

urlpatterns += review_urls
urlpatterns += product_urls
