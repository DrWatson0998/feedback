import factory
from faker import Faker
from reviewer.models import Category, Product, Review
from django.contrib.auth.models import User


fake = Faker()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category

    category = 'Все для дома'


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product

    name = 'Стол'
    category = factory.SubFactory(CategoryFactory)
    description = fake.text()
    product_image = 'sword-art-online-asuna-yuuki-5330.jpg'


class ReviewFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Review

    author = factory.SubFactory(UserFactory)
    product = factory.SubFactory(ProductFactory)
    review_text = fake.text()
    grade = 4
