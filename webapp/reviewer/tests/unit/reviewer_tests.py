import pytest
from django.urls import reverse
from reviewer.services import product_create, review_create
from reviewer.tests.test_helper import get_resp_status_code_from_url, get_resp_code_url_with_arguments
from django.contrib.auth.models import User


@pytest.mark.django_db
def test_product_create(category):
    product_record = {
        'description': 'Товар для дома',
        'name': 'Диван',
        'photo': 'sword-art-online-asuna-yuuki-5330.jpg'
    }
    assert product_create(product_record, category)


@pytest.mark.django_db
def test_review_create(product, user):
    review_record = {
        'review_text': 'Удобный столик',
        'grade': '4'
    }
    assert review_create(review_record, product, user)


@pytest.mark.django_db
def test_permission_main_page_by_unauthorized_user(client):
    assert get_resp_status_code_from_url(client, 'products_list') == 200


@pytest.mark.django_db
def test_permission_detail_product_page_by_unauthorized_user(client, product):
    assert get_resp_code_url_with_arguments(client, 'product_detail', product) == 200


@pytest.mark.django_db
def test_permission_create_product_unauthorized_user(client):
    assert get_resp_status_code_from_url(client, 'create_product') == 302


@pytest.mark.django_db
def test_permission_create_product_authorized_user(client, user):
    assert get_resp_status_code_from_url(client, 'create_product', user) == 403


@pytest.mark.django_db
def test_permission_update_product_unauthorized_user(client, product):
    assert get_resp_code_url_with_arguments(client, 'product_update', product) == 302


@pytest.mark.django_db
def test_permission_update_product_authorized_user(client, product, user):
    assert get_resp_code_url_with_arguments(client, 'product_update', product, user) == 403


@pytest.mark.django_db
def test_permission_delete_product_unauthorized_user(client, product):
    assert get_resp_code_url_with_arguments(client, 'delete_product', product) == 302


@pytest.mark.django_db
def test_permission_delete_product_authorized_user(client, product, user):
    assert get_resp_code_url_with_arguments(client, 'delete_product', product, user) == 403


@pytest.mark.django_db
def test_permission_create_product_by_superuser(admin_client):
    url = reverse('create_product')
    response = admin_client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_permission_create_review_unauthorized_user(client, product):
    assert get_resp_code_url_with_arguments(client, 'create_review', product) == 302


@pytest.mark.django_db
def test_permission_create_review_authorized_user(client, user, product):
    assert get_resp_code_url_with_arguments(client, 'create_review', product, user) == 200


@pytest.mark.django_db
def test_permission_update_review_unauthorized_user(client, review):
    assert get_resp_code_url_with_arguments(client, 'update_review', review) == 302


@pytest.mark.django_db
def test_permission_update_review_authorized_user(client, review_factory, user_factory):
    user = user_factory.create(username='user')
    review = review_factory.create(author=user_factory.create(username='author'))
    assert get_resp_code_url_with_arguments(client, 'update_review', review, user) == 403


@pytest.mark.django_db
def test_permission_update_review_authorized_author(client, review_factory, user_factory):
    author = user_factory.create(username='author')
    review = review_factory.create(author=author)
    assert get_resp_code_url_with_arguments(client, 'update_review', review, author) == 200


@pytest.mark.django_db
def test_permission_delete_review_unauthorized_user(client, review):
    assert get_resp_code_url_with_arguments(client, 'delete_review', review) == 302


@pytest.mark.django_db
def test_permission_delete_review_authorized_user(client, review_factory, user_factory):
    user = user_factory.create(username='user')
    review = review_factory.create(author=user_factory.create(username='author'))
    assert get_resp_code_url_with_arguments(client, 'delete_review', review, user) == 403


@pytest.mark.django_db
def test_permission_delete_review_authorized_author(client, review_factory, user_factory):
    author = user_factory.create(username='author')
    review = review_factory.create(author=author)
    assert get_resp_code_url_with_arguments(client, 'delete_review', review, author) == 302
