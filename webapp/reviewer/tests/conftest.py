from pytest_factoryboy import register
from reviewer.tests.factories import CategoryFactory, ProductFactory, UserFactory, ReviewFactory


register(CategoryFactory)
register(ProductFactory)
register(UserFactory)
register(ReviewFactory)

