from django.contrib import admin

from reviewer.models import Product, Review, Category


class ProductAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'category', ]
    list_filter = ['category']
    search_fields = ['name', 'category', ]
    fields = ['name', 'category', 'description', 'product_image']


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'category', ]
    fields = ['category', ]


class ReviewAdmin(admin.ModelAdmin):
    list_display = ['id', 'author', 'product', 'grade']
    search_fields = ['grade', ]
    fields = ['author', 'product', 'review_text', 'grade']


admin.site.register(Product, ProductAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Category, CategoryAdmin)

