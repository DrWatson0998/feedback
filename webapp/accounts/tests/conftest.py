from pytest import fixture
from django.contrib.auth.models import User

from accounts.models import Profile


@fixture
def user() -> User:
    return User.objects.create(
        username='admin',
        first_name='admin',
        last_name='admin',
        password='root',
        email='admin@gmail.com'
    )


@fixture
def profile(user) -> Profile:
    return Profile.objects.create(
        user=user,
        image='sword-art-online-asuna-yuuki-5330.jpg',
        about_user='Информация о пользователе'
    )
