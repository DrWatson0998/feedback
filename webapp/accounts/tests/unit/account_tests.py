import pytest
from accounts.services import register_user, clean_new_password_confirm, set_new_password_for_user
from reviewer.tests.test_helper import get_resp_status_code_from_url, get_resp_code_url_with_arguments


@pytest.mark.django_db
def test_register_user():
    user_record = {
        'first_name': 'admin',
        'last_name': 'adminovich',
        'username': 'admin',
        'password': 'root',
        'email': 'admin@gmail.com'
    }
    assert register_user(user_record)


@pytest.mark.django_db
def test_new_password_confirm():
    passwords = {
        'password': 'admin123',
        'password_confirm': 'admin123'
    }
    assert clean_new_password_confirm(**passwords)


@pytest.mark.django_db
def test_set_new_password(user):
    assert set_new_password_for_user(user, 'admin123')


@pytest.mark.django_db
def test_permission_login_unauthorized_user(client):
    assert get_resp_status_code_from_url(client, 'login') == 200


@pytest.mark.django_db
def test_permission_logout_unauthorized_user(client):
    assert get_resp_status_code_from_url(client, 'logout') == 302


@pytest.mark.django_db
def test_permission_logout_authorized_user(client, user):
    assert get_resp_status_code_from_url(client, 'logout', user) == 302


@pytest.mark.django_db
def test_permission_register_unauthorized_user(client):
    assert get_resp_status_code_from_url(client, 'register') == 200


@pytest.mark.django_db
def test_permission_view_profile_unauthorized_user(client, profile):
    assert get_resp_code_url_with_arguments(client, 'profile', profile) == 302


@pytest.mark.django_db
def test_permission_view_profile_authorized_user(client, profile, user):
    assert get_resp_code_url_with_arguments(client, 'profile', profile, user) == 200


@pytest.mark.django_db
def test_permission_view_profile_unauthorized_user(client, profile):
    assert get_resp_code_url_with_arguments(client, 'profile', profile) == 302


@pytest.mark.django_db
def test_permission_update_profile_unauthorized_user(client):
    assert get_resp_status_code_from_url(client, 'update_profile') == 302


@pytest.mark.django_db
def test_permission_update_profile_authorized_user(client, user, profile):
    assert get_resp_status_code_from_url(client, 'update_profile', user) == 200


@pytest.mark.django_db
def test_permission_change_password_unauthorized_user(client):
    assert get_resp_status_code_from_url(client, 'change_password') == 302


@pytest.mark.django_db
def test_permission_change_password_authorized_user(client, user):
    assert get_resp_status_code_from_url(client, 'change_password', user) == 200
