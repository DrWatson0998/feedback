from django import forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from accounts.models import Profile
from accounts.services import clean_new_password_confirm, set_new_password_for_user


class UserCreationForm(forms.ModelForm):
    first_name = forms.CharField(min_length=2, max_length=60, required=True)
    last_name = forms.CharField(min_length=2, max_length=60, required=False)
    email = forms.EmailField(min_length=6, max_length=100, required=True)
    password = forms.CharField(
        label='Пароль', strip=False, required=True, widget=forms.PasswordInput
    )
    password_confirm = forms.CharField(
        label='Потвердите пароль', strip=False, required=True, widget=forms.PasswordInput
    )

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('password_confirm')

        if password and password_confirm and password != password_confirm:
            raise ValidationError("Пароли не совпадают!")

    class Meta:
        model = get_user_model()
        fields = [
            'first_name', 'last_name', 'username', 'password', 'password_confirm', 'email'
        ]


class UserChangeForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name', 'email']


class ProfileChangeForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ['user', 'email']


class PasswordChangeForm(forms.ModelForm):
    password = forms.CharField(strip=False, widget=forms.PasswordInput)
    password_confirm = forms.CharField(strip=False, widget=forms.PasswordInput)
    password_old = forms.CharField(strip=False, widget=forms.PasswordInput)

    def clean_password_confirm(self):
        return clean_new_password_confirm(
            password=self.cleaned_data.get('password'),
            password_confirm=self.cleaned_data.get('password_confirm')
        )

    def clean_password_old(self):
        password_old = self.cleaned_data.get('password_old')
        if not self.instance.check_password(password_old):
            raise forms.ValidationError('Неправильно указан старый пароль')
        return password_old

    def save(self, commit=True):
        user = self.instance
        return set_new_password_for_user(
            user=user,
            password_confirm=self.cleaned_data['password_confirm']
        )

    class Meta:
        model = get_user_model()
        fields = ['password', 'password_confirm', 'password_old', ]

