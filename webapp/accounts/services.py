from django.contrib.auth.models import User
from accounts.models import Profile
from django import forms


def register_user(data: dict) -> User:
    user = User.objects.create(
        first_name=data.get('first_name'),
        last_name=data.get('last_name'),
        username=data.get('username'),
        password=data.get('password'),
        email=data.get('email')
    )
    Profile.objects.create(
        user=user,
        image='user_pics/default.jpg'
    )
    return user


def clean_new_password_confirm(password, password_confirm) -> str:
    if password and password_confirm and password != password_confirm:
        raise forms.ValidationError('Пароли не совпадают')
    return password_confirm


def set_new_password_for_user(user: User, password_confirm, commit=True) -> User:
    user.set_password(password_confirm)
    if commit:
        user.save()
    return user
